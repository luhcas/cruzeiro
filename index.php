<!DOCTYPE html>
<html>
  <?php include("includes/header.html"); ?>
  <style media="screen">
    @media only screen and (min-device-width : 1024px) and (max-width :4600px) {
      .soliciteOrcamento{
        position: fixed;
        float: left;
        left: 10px;
        top: 10px;
      }


    }
    @media only screen and (min-device-width : 10px) and (max-width :800px) {
      input, textarea{
        width: 100%;
      }
    }
  </style>
  <body>
    <?php include("includes/nav-head.html"); ?>

    <div class="container position-topo position-baixo">

        <div class="col-md-3 col-sm-12 col-xs-12">
          <?php include("includes/orcamento.php");?>
        </div>

        <div class="col-md-9 col-sm-12 col-xs-12">
          <?php include("includes/empresa.php");?>
          <?php include("includes/localizacao.php");?>
          <?php include("includes/servicos.php");?>
          <?php include("includes/frota.php");?>
          <?php include("includes/contato.php");?>

      </div>





    </div>
  </body>
</html>
