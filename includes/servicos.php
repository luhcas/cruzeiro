<div class="row" id="servicos" class="position-topo position-baixo">
  <div class="col-md-12">
    <h2 class="text-centered">Serviços</h2>
    <p class="text-centered">Contamos atualmente com os seguintes serviços:</p>
  </div>
  <div class="row">
    <div class="col-md-4">
      <h3 class="text-centered">Transporte Fretamento</h3>
      <p class="text-justify">Transporte de colaboradores de indústrias, fábricas e empresas.</p>
    </div>
    <div class="col-md-4">
      <h3 class="text-centered">Excursões Escolares</h3>
      <p class="text-justify">Transporte de alunos para eventos culturais, museus, teatros e feiras.</p>
    </div>
    <div class="col-md-4">
      <h3 class="text-centered">Turismo em Grupo</h3>
      <p class="text-justify">Corporativos (congressos, reuniões, exposições e feiras); comunidades evangélicas; matrimônios; acompanhamentos féritos entre outros.</p>
    </div>
  </div>
</div>
