<div class="row" id="contato" class="position-topo position-baixo">
  <div class="offset-md-2 col-md-8">
    <h2 class="text-centered">Contato</h2>
    <p class="text-justify">Fale conosco, envie sua mensagem pelo formulário de contato e retornaremos em breve:</p>
    <form class="" action="" method="post">
      <div class="row">
        <div class="col-md-2">
          <label for="nome">Nome</label>
        </div>
        <div class="col-md-10">
          <input type="text" id="nome" name="nome" class="form-group form-control col-md-4 col-sm-12 col-xs-12"><br>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2">
          <label for="email">E-mail</label>
        </div>
        <div class="col-md-10">
          <input type="text" id="email" name="email" class="form-group form-control col-md-4 col-sm-12 col-xs-12"><br>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2">
          <label for="telefone">Telefone</label>
        </div>
        <div class="col-md-10">
            <input type="text" id="telefone" name="telefone" class="form-group form-control col-md-4 col-sm-12 col-xs-12"><br>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2">
          <label for="assunto">Assunto</label>
        </div>
        <div class="col-md-10">
          <input type="text" id="assunto" name="assunto" class="form-group form-control col-md-4 col-sm-12 col-xs-12"><br>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <label for="mensagem">Mensagem</label><br>
          <textarea name="name" id="mensagem" rows="8" cols="80" class="form-group form-control col-md-4 col-sm-12 col-xs-12"></textarea>
        </div>
      </div>
    </form>
  </div>
</div>
