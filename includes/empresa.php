<div class="row" id="empresa" class="position-topo position-baixo">
  <div class="col-md-6">
    <h2 class="text-centered">Empresa</h2>
    <p class="text-justify">No final dos anos 70, o Sr. Benedito deu o primeiro passo para a realização do sonho de construir uma empresa de ônibus, adquirindo seu primeiro modelo. Próximo ao fim dos anos 80, em 17/03/1988, este sonho foi oficializado, surgindo a Cruzeiro Transportes e Turismo Ltda.
  Com muita dedicação e empenho, a empresa completa 27 anos de existência e atende diversos segmentos sempre prezando por um atendimento de qualidade, dedicação e eficiência na prestação de seus serviços.</p>
    <img src="assets/img/onibus-um.png" alt="onibus">
  </div>
  <div class="col-md-6">
    <h2 class="text-centered">Missão</h2>
    <p class="text-justify">Satisfazer nossos clientes e atendê-los com qualidade; transportar com segurança e manter o empenho e dedicação para cativá-los e torná-los parceiros.</p>
    <h2 class="text-centered">Visão</h2>
    <p class="text-justify">Ser referência no transporte fretamento e de turismo. Através de empenho e dedicação, assistir às demais regiões do país.</p>
    <h2 class="text-centered">Valores</h2>
    <p class="text-justify">Acreditamos que, sem Deus, nada podemos fazer e, o que fazemos é com Amor, Honestidade, Idoneidade, Responsabilidade, Respeito e Compromisso com nossos clientes, fornecedores, parceiros, colaboradores e concorrentes.</p>
  </div>

</div>
