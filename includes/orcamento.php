<div class="col-md-2 soliciteOrcamento position-topo position-baixo">
  <h6 class="text-centered">Solicite um Orçamento</h6>
  <form class="" action="cadastros/cadorcamento.php" method="post">
    <div class="row">
      <div class="col-md-12">
        <label for="nome">Serviço</label>
      </div>
      <div class="col-md-12">
        <input type="text" id="servico" name="servico"><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label for="nome">Nome</label>
      </div>
      <div class="col-md-12">
        <input type="text" id="nome" name="nome"><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label for="email">E-mail</label>
      </div>
      <div class="col-md-12">
        <input type="text" id="email" name="email"><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label for="telefone">Telefone</label>
      </div>
      <div class="col-md-12">
          <input type="text" id="telefone" name="telefone"><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label for="assunto">Cidade</label>
      </div>
      <div class="col-md-12">
        <input type="text" id="cidade" name="cidade"><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label for="dataViagem">Data da Viagem</label>
      </div>
      <div class="col-md-12">
        <input type="date" id="dataViagem" name="dataViagem"><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label for="mensagem">Mensagem</label><br>
        <textarea name="mensagem" id="mensagem" rows="4" cols="20"></textarea>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <input type="submit" value="Enviar">
      </div>
    </div>
  </form>
</div>
