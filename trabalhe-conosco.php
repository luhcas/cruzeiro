<!DOCTYPE html>
<html>
  <?php include("includes/header.html"); ?>
  <body>
    <?php include("includes/nav-head.html"); ?>
    <div class="container position-topo position-baixo">

      <div class="row" id="frota" class="position-topo position-baixo">
        <h2 class="text-centered">Contato</h2>
        <p class="text-justify">Para fazer parte da equipe Cruzeiro, preencha o formulário abaixo, não esquecendo de anexar o currículo.</p>
        <div class="offset-md-2 col-md-8">
          <div class="row">
            <div class="col-md-2">
              <label for="nome">Nome</label>
            </div>
            <div class="col-md-10">
              <input type="text" id="nome" name="nome" class="form-group form-control col-md-4 col-sm-12 col-xs-12"><br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <label for="area">Área desejada</label>
            </div>
            <div class="col-md-10">
              <input type="text" id="area" name="area" class="form-group form-control col-md-4 col-sm-12 col-xs-12"><br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <label for="curriculo">Anexe seu Currículo</label>
            </div>
            <div class="col-md-10">
              <input type="file" id="curriculo" name="curriculo" class="form-group form-control col-md-4 col-sm-12 col-xs-12"><br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
